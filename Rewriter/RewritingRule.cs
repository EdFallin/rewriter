﻿
namespace Rewriter
{
    /* a single regular transformation to apply */

    public class RewritingRule
    {
        #region Properties

        public string From { get; set; }
        public string To { get; set; }

        public bool DoApplyAcrossLines { get; set; }

        // if true, rewriting should be repeated until no matches are left 
        public bool DoExhaust { get; set; }

        #endregion Properties


        #region Constructors

        /* when using an object in a way where empties might be created by client code 
         * (for instance, in WPF's DataGrid), a no-args ctor must exist */
        public RewritingRule() {
            /* no operations */
        }

        public RewritingRule(string from, string to) : this(from, to, false, false) {
            /* no operations */
        }

        public RewritingRule(string from, string to, bool doApplyAcrossLines) : this(from, to, doApplyAcrossLines, false) {
            /* no operations */
        }

        public RewritingRule(string from, string to, bool doApplyAcrossLines, bool doExhaust) {
            this.From = from;
            this.To = to;
            this.DoApplyAcrossLines = doApplyAcrossLines;
            this.DoExhaust = doExhaust;
        }

        #endregion Constructors
    }
}
