﻿
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Windows;
using System.Windows.Input;

namespace Rewriter
{
    public partial class MainWindow : Window
    {
        #region Fields

        ViewModel vm = new ViewModel();

        #endregion Fields


        #region Properties

        public static RoutedUICommand RewriteText { get; set; } = new RoutedUICommand();
        public static RoutedUICommand ClearState { get; set; } = new RoutedUICommand();
        public static RoutedUICommand ClearFile { get; set; } = new RoutedUICommand();
        public static RoutedUICommand ClearRules { get; set; } = new RoutedUICommand();
        public static RoutedUICommand ExitApp { get; set; } = new RoutedUICommand();

        #endregion Properties


        #region Constructors

        public MainWindow() {
            this.DataContext = vm;
            InitializeComponent();
        }

        #endregion Constructors


        #region Command event handlers

        public void CanRewriteText(object sender, CanExecuteRoutedEventArgs e) {
            e.CanExecute = (vm.HasFile) && (vm.Rules.Count > 0);
        }

        public void EnactRewriteText(object sender, ExecutedRoutedEventArgs e) {
            vm.RewriteFileText();
        }

        public void CanClearState(object sender, CanExecuteRoutedEventArgs e) {
            // no state to clear unless one of these is true 
            e.CanExecute = (vm.HasFile) || (vm.Rules.Count > 0);
        }

        public void EnactClearState(object sender, ExecutedRoutedEventArgs e) {
            vm.StoredFile = null;  // IO-safe 
            vm.Rules.Clear();
        }

        public void CanClearFile(object sender, CanExecuteRoutedEventArgs e) {
            e.CanExecute = vm.HasFile;
        }

        public void EnactClearFile(object sender, ExecutedRoutedEventArgs e) {
            vm.StoredFile = null;
        }

        public void CanClearRules(object sender, CanExecuteRoutedEventArgs e) {
            e.CanExecute = (vm.Rules.Count > 0);
        }

        public void EnactClearRules(object sender, ExecutedRoutedEventArgs e) {
            vm.Rules.Clear();
        }

        public void CanExitApp(object sender, CanExecuteRoutedEventArgs e) {
            e.CanExecute = true;
        }

        public void EnactExitApp(object sender, ExecutedRoutedEventArgs e) {
            // the safest way to do this, although maybe overkill 
            Application.Current.Shutdown();
        }

        #endregion Command event handlers


        #region Event handlers and dependencies

        private void FileDropLabel_Drop(object sender, DragEventArgs e)  /* verified */  {
            if (NoFileDropIsPresent(e)) {
                return;
            }

            string filePath = FromDropEventArgsToFilePath(e);
            vm.StoredFile = filePath;  // validated by ViewModel 

            // hackish, but this is the short way to make the changed property _usually_ 
            // fire updating events for other controls (especially ClearFileButton); 
            // if working on this app much more, should try to improve on this 
            this.FileDropLabel.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
        }

        private void RulesDataGrid_Drop(object sender, DragEventArgs e)  /* verified */  {
            if (NoFileDropIsPresent(e)) {
                return;
            }

            // rules expected as JSON in file 
            string filePath = FromDropEventArgsToFilePath(e);
            string json = File.ReadAllText(filePath);
            List<RewritingRule> rules = FromJsonTextToRewritingRules(json);

            // dropped rules replace existing 
            vm.Rules.Clear();
            rules.ForEach(x => vm.Rules.Add(x));
        }

        #region Dependencies

        private bool NoFileDropIsPresent(DragEventArgs e) {
            return !e.Data.GetDataPresent(DataFormats.FileDrop);
        }

        private string FromDropEventArgsToFilePath(DragEventArgs e) {
            object dropped = e.Data.GetData(DataFormats.FileDrop);
            object[] items = dropped as object[];
            string first = items[0] as string;

            return first;
        }

        private List<RewritingRule> FromJsonTextToRewritingRules(string json) {
            // JSON in JS style, properties in CS style 
            JsonSerializerOptions options = new JsonSerializerOptions();
            options.PropertyNameCaseInsensitive = true;

            List<RewritingRule> rules = JsonSerializer.Deserialize<List<RewritingRule>>(json, options);
            return rules;
        }

        #endregion Dependencies

        #endregion Event handlers and dependencies
    }
}
