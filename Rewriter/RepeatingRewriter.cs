﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Rewriter
{
    public class RepeatingRewriter : ARewriter
    {
        protected override string PrimaryRewrite(string text, string from, string to, bool doApplyAcrossLines)  /* verified */  {
            RegexOptions options = RegexOptions.None;

            if (doApplyAcrossLines) {
                // adding cross-lines mode, presumably called "single line" because it treats entire text as a single line 
                options = options |= RegexOptions.Singleline;
            }

            // rewriting should always be tried at least once 
            do {
                text = Regex.Replace(text, from, to, options);
            } while (Regex.IsMatch(text, from, options));

            return text;
        }

        public RepeatingRewriter(RewritingRule rule) : base(rule) {
            /* no operations */
        }
    }
}
