﻿
using System.Text.RegularExpressions;

namespace Rewriter
{
    public class OneTimeRewriter : ARewriter
    {
        protected override string PrimaryRewrite(string text, string from, string to, bool doApplyAcrossLines)  /* verified */  {
            RegexOptions options = RegexOptions.None;

            if (doApplyAcrossLines) {
                // adding cross-lines mode, presumably called "single line" because it treats entire text as a single line 
                options = options |= RegexOptions.Singleline;
            }

            text = Regex.Replace(text, from, to, options);
            return text;
        }

        public OneTimeRewriter(RewritingRule rule) : base(rule) {
            /* no operations */
        }
    }
}
