﻿
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Windows;
using Microsoft.Win32;  // for SaveFileDialog, what a crock 
using IObservable = System.ComponentModel.INotifyPropertyChanged;

namespace Rewriter
{
    public class ViewModel : IObservable
    {
        #region Definitions

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion Definitions


        #region Fields

        TextRewriter _tr = new TextRewriter();
        bool _hasFile;
        string _storedFile;
        bool _didThrow;

        #endregion Fields


        #region Properties

        public ObservableCollection<RewritingRule> Rules { get => _tr.Rules; }

        // not visible, so not IObservable 
        public string Text { get; set; }

        public string StoredFile {
            get => _storedFile;
            set {
                _storedFile = value;
                this.HasFile = true;

                // stored file path can be null after clearing or if folder dropped 
                if (!File.Exists(value)) {
                    _storedFile = null;
                    this.HasFile = false;
                }

                OnPropertyChanged(nameof(this.StoredFile));
            }
        }

        public string FileToStore { get; set; }

        public bool HasFile {
            get => _hasFile;
            set {
                _hasFile = value;
                OnPropertyChanged(nameof(this.HasFile));
            }
        }

        public bool DidThrow {
            get => _didThrow;
            set {
                _didThrow = value;
                OnPropertyChanged(nameof(this.DidThrow));
            }
        }

        #endregion Properties


        #region Constructors

        public ViewModel() {
            /* no operations */
        }

        #endregion Constructors


        #region Methods

        public void RewriteFileText()  /* verified */  {
            GetFileToStore();

            if (this.FileToStore == null) {
                return;
            }

            DidThrow = false;

            try {
                string text = File.ReadAllText(this.StoredFile);
                text = _tr.RewriteText(text);
                File.WriteAllText(this.FileToStore, text);
            }
            catch {
                DidThrow = true;
            }
        }

        private void GetFileToStore()  /* verified */  {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.AddExtension = true;
            dialog.CheckPathExists = true;
            dialog.DefaultExt = ".txt";
            dialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            dialog.OverwritePrompt = true;
            dialog.Title = "Save rewrites as:";
            dialog.ValidateNames = true;

            bool? result = dialog.ShowDialog(Application.Current.MainWindow);
            bool fileWasChosen = result ?? false;

            // file must be chosen each time 
            if (fileWasChosen) {
                this.FileToStore = dialog.FileName;
            }
            else {
                this.FileToStore = null;
            }
        }

        #endregion Methods


        #region IObservable

        protected void OnPropertyChanged(string propertyName) {
            if (PropertyChanged != null) {
                PropertyChangedEventArgs args = new PropertyChangedEventArgs(propertyName);
                PropertyChanged(this, args);
            }
        }

        #endregion IObservable
    }
}
