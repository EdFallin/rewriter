﻿
using System.Text;

namespace Rewriter
{
    public abstract class ARewriter
    {
        #region Definitions

        const char ESC_CHAR = '\\';
        readonly char[] ESCD_CHARS = { 'r', 'n', 't' };
        readonly char[] ESC_SEQS = { '\r', '\n', '\t' };

        #endregion Definitions


        #region Properties

        public RewritingRule Rule { get; set; }

        #endregion Properties


        #region Constructors

        /* concrete, but still has to be implemented on concrete classes! */
        public ARewriter(RewritingRule rule) {
            this.Rule = rule;
        }

        #endregion Constructors


        #region Key method and dependencies

        // Template Method pattern: concrete template method 
        public string Rewrite(string text) {
            // nulls can't be passed to Regex.Replace() 
            string from = this.Rule.From ?? string.Empty;
            string to = this.Rule.To ?? string.Empty;

            bool doApplyAcrossLines = this.Rule.DoApplyAcrossLines;

            text = this.PrimaryRewrite(text, from, to, doApplyAcrossLines);
            text = this.ConvertEscapeSequences(text);

            return text;
        }

        // - of Rewrite(): Template Method pattern: abstract dependency of Rewrite(), implemented by subclasses 
        abstract protected string PrimaryRewrite(string text, string from, string to, bool doApplyAcrossLines);

        // - of Rewrite(): Template Method pattern: concrete dependency of Rewrite(), always needed, implemented here 
        protected string ConvertEscapeSequences(string text) {
            /* strings entered from the UI have the backslashes in escape sequences doubled; 
             * it seems the only way to produce actual escape sequences is to work char by char */

            StringBuilder asBuilder = new StringBuilder(text);

            // each backslash that's followed by one of the escape-sequence characters 
            // is replaced with the escape sequence, and the stray following char is dropped; 
            // loop condition is .Length - 2 to stay in range since in loop, @next is @at + 1 
            for (int at = 0; at < asBuilder.Length - 2; at++) {
                if (asBuilder[at] == ESC_CHAR) {
                    int next = at + 1;

                    for (int of = 0; of < ESCD_CHARS.Length; of++) {
                        if (asBuilder[next] == ESCD_CHARS[of]) {
                            asBuilder[at] = ESC_SEQS[of];
                            asBuilder.Remove(next, 1);
                        }
                    }
                }
            }

            text = asBuilder.ToString();
            return text;
        }

        #endregion Key method and dependencies
    }
}
