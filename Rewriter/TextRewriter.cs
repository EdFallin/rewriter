﻿
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Rewriter
{
    public class TextRewriter
    {
        #region Fields

        ObservableCollection<RewritingRule> _rules = new ObservableCollection<RewritingRule>();

        #endregion Fields


        #region Properties

        public ObservableCollection<RewritingRule> Rules { get => _rules; }  // no setter: items changed, not collection 

        #endregion Properties


        #region Constructors

        public TextRewriter() {
            /* no operations */
        }

        #endregion Constructors


        #region Methods

        public string RewriteText(string text)  /* verified */  {
            List<ARewriter> rewriters = RulesToWriters();
            text = PerformAllRewrites(text, rewriters);
            return text;
        }

        // - of RewriteText() 
        private List<ARewriter> RulesToWriters()  /* verified */  {
            List<ARewriter> rewriters = new List<ARewriter>(_rules.Count);

            foreach (RewritingRule rule in _rules) {
                ARewriter rewriter = this.RewriterFromRewritingRule(rule);
                rewriters.Add(rewriter);
            }

            return rewriters;
        }

        // - - of RulesToWriters() 
        private ARewriter RewriterFromRewritingRule(RewritingRule rule)  /* verified */  {
            ARewriter rewriter = null;

            if (!rule.DoExhaust) {
                rewriter = new OneTimeRewriter(rule);
            }
            else {
                rewriter = new RepeatingRewriter(rule);
            }

            return rewriter;
        }

        // - of RewriteText() 
        private string PerformAllRewrites(string text, List<ARewriter> rewriters)  /* verified */  {
            foreach (ARewriter rewriter in rewriters) {
                text = rewriter.Rewrite(text);
            }

            return text;
        }

        #endregion Methods

    }
}
